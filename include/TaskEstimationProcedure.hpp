#pragma once

#include <vector>
#include <string>

#include "FuzzyEstimation.hpp"

class TaskEstimationProcedure
{
public:
    TaskEstimationProcedure(const std::string& taskName);

    void execute();
    FuzzyEstimation fetchAverageEstimation();
    
private:
    void greetUser();

    void readEstimationsFromUsers();
    FuzzyEstimation readEstimation();
    void queryForContinue();

    void printResults();

    std::string taskName;
    bool running = true;
    std::vector<FuzzyEstimation> estimations;
};
