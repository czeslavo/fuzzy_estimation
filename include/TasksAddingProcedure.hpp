#pragma once

#include <string>
#include <vector>

class TasksAddingProcedure
{
public:
    void execute();
    std::vector<std::string> fetchTasks();

private:
    void greetUser();
    void readTasksFromUser();
    std::string readTask();
    void queryForContinue();
    void printResult();

    bool running = true;
    std::vector<std::string> tasks;
};


