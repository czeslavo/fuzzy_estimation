#pragma once

#include <map>
#include <string>
#include <optional>

#include "FuzzyEstimation.hpp"

class EstimatedProjectReporter
{
public:
    EstimatedProjectReporter(std::map<std::string, FuzzyEstimation> estimatedTasks);

    void printReport();
    void saveToFileIfNeeded();

private:
    FuzzyEstimation getProjectEstimation();
    int getDegreeIfNotSet();
    int sharpen(const FuzzyEstimation& est, int degree);

    void saveToFile();
    std::string serializeEstimations();

    std::optional<int> degree;
    std::map<std::string, FuzzyEstimation> tasks;
};
