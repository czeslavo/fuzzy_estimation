#pragma once

#include <ostream>
#include <numeric>

struct FuzzyEstimation 
{
    int optimistic;
    int mostLikely;
    int pessimistic;
};

template <typename Int>
FuzzyEstimation operator/(const FuzzyEstimation& lhs, Int rhs)
{
    return {
        lhs.optimistic / rhs,
        lhs.mostLikely / rhs,
        lhs.pessimistic / rhs
    };
}

FuzzyEstimation operator+(const FuzzyEstimation& lhs, const FuzzyEstimation& rhs);

std::ostream& operator<<(std::ostream& os, const FuzzyEstimation& est);
int triangle(const FuzzyEstimation& est, const float slope);


