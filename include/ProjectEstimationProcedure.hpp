#pragma once

#include <vector>
#include <string>
#include <map>

#include "FuzzyEstimation.hpp"

class ProjectEstimationProcedure
{
public:
    ProjectEstimationProcedure(std::vector<std::string> tasks);

    void execute();
    std::map<std::string, FuzzyEstimation> fetchEstimatedTasks();
    
private:
    void estimateAllTasks();

    std::vector<std::string> tasks;
    std::map<std::string, FuzzyEstimation> estimatedTasks;
};
