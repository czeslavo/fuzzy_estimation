#include "TaskEstimationProcedure.hpp"

#include <string>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <numeric>

TaskEstimationProcedure::TaskEstimationProcedure(const std::string& taskName) : taskName(taskName) 
{
}

void TaskEstimationProcedure::execute()
{
    greetUser();
    readEstimationsFromUsers();
    printResults();
}

void TaskEstimationProcedure::greetUser()
{
    std::cout << "We'll estimate task: " << taskName << ".\n"; 
}

void TaskEstimationProcedure::readEstimationsFromUsers()
{
    int expertNumber = 1;
    while (running)
    {
        std::cout << "Expert no. " << expertNumber++ << '\n';
        estimations.emplace_back(readEstimation());
        queryForContinue();
    }
}

int readNumber(const std::string& name)
{
    std::cout << name << ": ";
    std::string buff;
    std::cin >> buff;
    try {
        return boost::lexical_cast<unsigned>(buff);
    } catch (...) {
        std::cerr << "Should give number!\n";
        return readNumber(name);    
    }
}

FuzzyEstimation TaskEstimationProcedure::readEstimation()
{
    return {
        readNumber("Optimistic"),
        readNumber("Most likely"),
        readNumber("Pessimistic")
    };
}

void TaskEstimationProcedure::queryForContinue()
{
    std::cout << "Add another estimation? [y/n]\n";
    std::string cont;
    std::cin >> cont;

    running = cont == "y";
}

void TaskEstimationProcedure::printResults()
{
    const auto averageEstimation = fetchAverageEstimation();
    std::cout << taskName << " estimation: " << averageEstimation << '\n';
}

FuzzyEstimation TaskEstimationProcedure::fetchAverageEstimation()
{
    if (not estimations.empty())
    {
        const auto sum = std::accumulate(estimations.cbegin(), estimations.cend(), FuzzyEstimation{0, 0, 0});
        return sum / static_cast<int>(estimations.size());
    }
    else
    {
        return {0, 0, 0};
    }
}


