#include "EstimatedProjectReporter.hpp"

#include <iostream>
#include <vector>
#include <functional>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <sstream>
#include <boost/filesystem.hpp>

EstimatedProjectReporter::EstimatedProjectReporter(std::map<std::string, FuzzyEstimation> estimatedTasks)
    : tasks(std::move(estimatedTasks))
{
}

void EstimatedProjectReporter::printReport()
{
    getDegreeIfNotSet();

    const auto projectEstimation = getProjectEstimation();
    std::cout << "Project estimated for: " << projectEstimation << " -> " << sharpen(projectEstimation, degree.value_or(1)) << '\n';

    for (const auto& [taskName, estimation] : tasks)
    {
        std::cout << taskName << ": " << estimation << " -> " << sharpen(estimation, degree.value_or(1)) << '\n';
    }
}

FuzzyEstimation EstimatedProjectReporter::getProjectEstimation()
{
    std::vector<FuzzyEstimation> estimations;
    std::transform(tasks.cbegin(), tasks.cend(), std::back_inserter(estimations), [](const auto &e) {
        return e.second;
    });
    
    return std::accumulate(estimations.cbegin(), estimations.cend(), FuzzyEstimation{0, 0, 0});
}  

int EstimatedProjectReporter::getDegreeIfNotSet()
{
    if (not degree)
    {
        std::cout << "Degree for fuzzy number sharpening [1-4]: ";
        std::string buff;
        std::cin >> buff;
        std::cin.clear();
        try {
            degree = boost::lexical_cast<int>(buff);
        } catch (...) {
            getDegreeIfNotSet();
        }
    }
}

int EstimatedProjectReporter::sharpen(const FuzzyEstimation& e, int degree)
{
    auto leftRightSum = [&] { return e.optimistic + e.pessimistic; };
    switch (degree)
    {
        case 1: return e.mostLikely;
        case 2: return (leftRightSum() + e.mostLikely) / 3;
        case 3: return (leftRightSum() + e.mostLikely * 2) / 4;
        case 4: return (leftRightSum() + e.mostLikely * 4) / 6;
        default: return e.mostLikely;
    }
}

void EstimatedProjectReporter::saveToFileIfNeeded()
{
    std::cout << "Save estimations to file? [y/n]\n";
    std::string buff;
    std::cin >> buff;
    if (buff == "y")
    {
        saveToFile();
    }
}

void EstimatedProjectReporter::saveToFile()
{
    std::cout << "Filename: ";
    std::string buff;
    std::cin >> buff;
    
    std::cout << "Saving to " << boost::filesystem::path(buff) << '\n';
    std::ofstream f(buff);
    if (not f.is_open())
        std::cerr << "Couldn't open file for writing";

    f << serializeEstimations();
    f.close();
}

std::string EstimatedProjectReporter::serializeEstimations()
{
    getDegreeIfNotSet();

    std::stringstream s;

    const auto projectEstimation = getProjectEstimation();
    s << "Project estimated for: " << projectEstimation << " -> " << sharpen(projectEstimation, degree.value_or(1)) << '\n';

    for (const auto& [taskName, estimation] : tasks)
    {
        s << taskName << ": " << estimation << " -> " << sharpen(estimation, degree.value_or(1)) << '\n';
    }

    return s.str();
}

