#include "TasksAddingProcedure.hpp"

#include <iostream>

void TasksAddingProcedure::execute()
{
    greetUser();
    readTasksFromUser();
    printResult();
}

void TasksAddingProcedure::greetUser()
{
    std::cout << "We'll add tasks for our project.\n";
}

void TasksAddingProcedure::readTasksFromUser()
{
    while (running)
    {
        tasks.emplace_back(readTask());
        queryForContinue();
    }
}

std::string TasksAddingProcedure::readTask()
{
    std::cout << "New task title: ";
    
    std::string buff;
    std::getline(std::cin, buff);
    std::cin.clear();

    return buff;
}

void TasksAddingProcedure::queryForContinue()
{
    std::cout << "Add another task? [y/n]\n";
    
    std::string cont;
    std::getline(std::cin, cont);
    std::cin.clear();

    running = cont == "y";
}


void TasksAddingProcedure::printResult()
{
    std::cout << "Added " << tasks.size() << " tasks:\n";
    
    for (auto i = 0u; i < tasks.size(); ++i)
        std::cout << i + 1 << ". " << tasks[i] << '\n';
}

std::vector<std::string> TasksAddingProcedure::fetchTasks() 
{
    return tasks;
}

