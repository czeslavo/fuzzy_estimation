#include "FuzzyEstimation.hpp"

FuzzyEstimation operator+(const FuzzyEstimation& lhs, const FuzzyEstimation& rhs)
{
    return {
        lhs.optimistic + rhs.optimistic,
        lhs.mostLikely + rhs.mostLikely,
        lhs.pessimistic + rhs.pessimistic
    };
}

std::ostream& operator<<(std::ostream& os, const FuzzyEstimation& est)
{
    os << "(" << est.optimistic << ", " << est.mostLikely << ", " << est.pessimistic << ")";
}

int triangle(const FuzzyEstimation& est, const float slope)
{
    float divider = 3.f + slope;
    return (est.optimistic + est.mostLikely * (slope + 1.f) + est.pessimistic) / divider;
}
