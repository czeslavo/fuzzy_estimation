#include "ProjectEstimationProcedure.hpp"
#include "TaskEstimationProcedure.hpp"

ProjectEstimationProcedure::ProjectEstimationProcedure(std::vector<std::string> tasks)
    : tasks(std::move(tasks))
{
}

void ProjectEstimationProcedure::execute()
{
    estimateAllTasks(); 
}

std::map<std::string, FuzzyEstimation> ProjectEstimationProcedure::fetchEstimatedTasks()
{
    return estimatedTasks;
}

void ProjectEstimationProcedure::estimateAllTasks()
{
    for (const auto& task : tasks)
    {
        TaskEstimationProcedure estimationProcedure{task};
        estimationProcedure.execute();
        estimatedTasks.emplace(task, estimationProcedure.fetchAverageEstimation());
    }
}
