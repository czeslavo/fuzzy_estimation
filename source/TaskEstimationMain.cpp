#include "TasksAddingProcedure.hpp"
#include "ProjectEstimationProcedure.hpp"
#include "EstimatedProjectReporter.hpp"

int main()
{
    TasksAddingProcedure addingProcedure;
    addingProcedure.execute();
    const auto tasks = addingProcedure.fetchTasks();
   
    ProjectEstimationProcedure estimationProcedure(tasks);
    estimationProcedure.execute();
    const auto estimatedTasks = estimationProcedure.fetchEstimatedTasks();

    EstimatedProjectReporter reporter(estimatedTasks);
    reporter.printReport();
    reporter.saveToFileIfNeeded();
}
